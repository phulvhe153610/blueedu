﻿using Group1_CourseOnline.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace Group1_CourseOnline.Pages
{
    public class RegisterModel : PageModel
    {
        public readonly SWP391_BlueEduContext _db;
        public RegisterModel(SWP391_BlueEduContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Account Account { get; set; }
        public Customer Customer { get; set; }

        public IActionResult OnGet()
        {
            // Check if the user is already logged in
            string? customer = HttpContext.Session.GetString("customer");
            string? admin = HttpContext.Session.GetString("admin");
            if (customer != null || admin != null)
            {
                return Redirect("/Index");
            }

            ViewData["title"] = "Register";
            return Page();
        }


        // POST: /Signup
        public async Task<IActionResult> OnPost(string? confirm)
        {
            if (Account.Email == null || Account.Password == null || Account.Customer.FirstName == null || Account.Customer.LastName == null)
            {
                TempData["msg"] = "You must enter all the fields";
                return Page();
            }
            string pattern = @"^[^@\s]+@[^@\s]+\.[^@\s]+$";

            if (!Regex.IsMatch(Account.Email, pattern))
            {
                TempData["msg"] = "Please enter the correct email format ex: abc@gmail.com";
                return Page();
            }
            if (!confirm.Equals(Account.Password))
            {
                TempData["msg"] = "Password does not match";
                return Page();
            }
            var accountExists = await _db.Accounts.AnyAsync(a => a.Email == Account.Email);
            if (accountExists)
            {
                TempData["msg"] = "An account with this email already exists. Please choose another email";
                return Page();
            }



            var newCus = new Customer()
            {
                LastName = Account.Customer.LastName,
                FirstName = Account.Customer.FirstName,
                Phone = null,
                BirthDate = null,
                Address = null,
                Avatar = null,
                Status = true
            };

            _db.Customers.AddAsync(newCus);
            await _db.SaveChangesAsync();

            var newAcc = new Account()
            {
                Email = Account.Email,
                Password = Account.Password,
                CustomerId = newCus.CustomerId, // Lấy giá trị CustomerId đã được tạo tự động
                Role = 2
            };

            _db.Accounts.AddAsync(newAcc);
            await _db.SaveChangesAsync();

            return RedirectToPage("/Login");
        }
    }
}