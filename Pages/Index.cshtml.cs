using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Group1_CourseOnline.Pages
{
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
        }
		public async Task<IActionResult> OnPostAsync()
		{
			var imageFile = Request.Form.Files.FirstOrDefault();

			if (imageFile != null && imageFile.Length > 0)
			{
				// Process the file
				var imagePath = "D:\\Summer2023\\SWP391\\Group1_CourseOnline\\wwwroot\\img\\" + imageFile.FileName;

				using (var stream = new FileStream(imagePath, FileMode.Create))
				{
					await imageFile.CopyToAsync(stream);
				}
			}

			// Redirect to a different page or return a response
			return RedirectToPage("/Index");
		}
	}
}
