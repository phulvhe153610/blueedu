using Group1_CourseOnline.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace Group1_CourseOnline.Pages
{
    public class LoginModel : PageModel
    {
        private readonly SWP391_BlueEduContext _db;
        public LoginModel(SWP391_BlueEduContext db)
        {
            _db = db;
        }
        [BindProperty]
        public Account Account { get; set; }

        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("admin") != null || HttpContext.Session.GetString("customer") != null)
            {
                return RedirectToPage("./Admin/Index");
            }
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var member = await _db.Accounts.SingleOrDefaultAsync(a => a.Email.Equals(Account.Email) && a.Password.Equals(Account.Password));
            if (member != null)
            {
                HttpContext.Session.SetString("Account", JsonSerializer.Serialize(member));
                if (member.Role == 1)
                {
                    HttpContext.Session.SetString("admin", JsonSerializer.Serialize(member));
                    return RedirectToPage("./Admin/Index");
                }
                if (member.Role == 2)
                {
                    HttpContext.Session.SetString("customer", JsonSerializer.Serialize(member));
                    HttpContext.Session.SetString("email", member.Email);
                    return RedirectToPage("/Index");
                }
            }

            TempData["msg"] = "Email or password invalid.";
            return Page();

        }
    }
}
